<?php 

namespace App\Controllers;

use CodeIgniter\Controller;

class DashboardController extends Controller
{
	public function index()
	{
		$session = session();
		echo 'Hello user: '.$session->get('name');
	}
}