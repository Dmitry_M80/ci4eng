<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\UserModel;


/**
 * Login controller
 */
class LoginController extends Controller
{
	public function index()
	{
		helper(['form']);
		echo view('login');
	}

	public function signin()
	{
		$session = session();
		$model = new UserModel;

		$email = $this->request->getVar('email');
		$password = $this->request->getVar('password'); // TODO: Var?

		$data = $model->where('email', $email)->first();

		if ($data) {
			$pass = $data['password'];
			$pwd_verify = password_verify($password, $pass);

			if ($pwd_verify) {
				$sess_data = [
					'id' => $data['id'],
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'isSignedIn' => TRUE
				];

				$session->set($sess_data);

				return redirect()->to('/dashboard');
			} else {
				echo "no1"; die();
				$session->setFlashdata('msg', 'Wrong password');
				return redirect()->to(base_url('login'));
			}
		} else {
			// echo "no2"; die();
			$session->setFlashdata('msg', 'Wrong password 2');
			return redirect()->to(base_url('login'));
		}
	}
}
