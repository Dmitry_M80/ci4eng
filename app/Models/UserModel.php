<?php

namespace App\Models;

use CodeIgniter\Model;


/**
 * Model for auth
 */
class UserModel extends Model
{
	protected $table = 'users';
	protected $primaryKey = 'id';
	protected $allowedFields = [
		'name',
		'email',
		'password',
		'created_at'
	];
}